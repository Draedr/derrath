﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using System.Collections.Generic;

namespace MiniGame
{
    public class Game1 : Game
    {

        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public SceneManager sceneManager;

        public SpriteFont font;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            sceneManager = new SceneManager();

            sceneManager.RegisterScene( new TitleScene( this ), "titleScene" );
            sceneManager.RegisterScene( new MainScene( this ), "mainScene" );

            sceneManager.Start( "titleScene" );
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
            font = Content.Load<SpriteFont>("Content/textTest");

            sceneManager.Current().LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            sceneManager.Current().Update( gameTime );

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            sceneManager.Current().Draw( gameTime );

            base.Draw(gameTime);
        }
    
    }
}
