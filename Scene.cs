using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.Collections;
using System.Collections.Generic;

namespace MiniGame
{
    public abstract class Scene
    {
        protected Game1 _game { get; set; } 
        
        public bool initialized { get; set; } = false;

        public Scene ( Game1 game ) 
        {
            _game = game;
        }

        public abstract void Initialize ();

        public abstract void MoveTo ();

        public abstract void MoveFrom ();

        public abstract void Close ();

        public abstract void LoadContent ();

        public abstract void Update (GameTime gameTime);

        public abstract void Draw (GameTime gameTime);

    }
}