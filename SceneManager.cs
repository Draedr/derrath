using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.Collections;
using System.Collections.Generic;

namespace MiniGame
{

    public class SceneManager
    {
        private string currentScene { get; set; } = "main";

        private Dictionary<string, Scene> _scenes;
        
        private ArrayList sceneHistory;

        public SceneManager ()
        {
            _scenes = new Dictionary<string, Scene>();
            sceneHistory = new ArrayList();
        }

        public Scene Current ()
        {
            return _scenes[ currentScene ];
        }

        public void Start ( string name ) 
        {
            currentScene = name;

            if( ! Current().initialized )
            {
                Current().Initialize();
            }
        }

        public void LoadAllContent ()
        {
            foreach( Scene value in _scenes.Values )
            {
                value.LoadContent();
            }
        }

        public void ChangeScene ( string name )
        {
            if( _scenes.ContainsKey( name ) ) {

                Current().MoveTo();
                sceneHistory.Add( name );
                
                currentScene = name;

                if( ! Current().initialized )
                {
                    Current().Initialize();
                }

                Current().MoveFrom();

            }
        }

        public void RegisterScene( Scene scene, string name )
        {
            _scenes.Add( name, scene );
        }

        public void UnregisterScene( string name )
        {
            if( name == currentScene ) 
            {
                _scenes[ name ].Close();
                
            }

            _scenes.Remove( name );
        }

    }

}