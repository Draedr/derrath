using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

using System.Collections;
using System.Collections.Generic;

namespace MiniGame
{
    public class TitleScene : Scene
    {
        KeyboardState ks1, ks2;
        SpriteFont font;

        public TitleScene ( Game1 _game ) : base ( _game )
        {}

        public override void LoadContent ()
        {
            
        }

        public override void Update ( GameTime gt )
        {
            ks1 = Keyboard.GetState();

            if( ks1.IsKeyDown( Keys.F ) && ks2.IsKeyUp( Keys.F ) )
            {
                _game.sceneManager.ChangeScene("mainScene");
            }

            ks2 = ks1;

        }

        public override void Draw ( GameTime gt )
        {
            _game.spriteBatch.Begin();
            
            //_game.spriteBatch.DrawString( font, "TitleScene", new Vector2(100, 100), Color.Black);
            
            _game.spriteBatch.End();
        }

        public override void MoveFrom() {
            System.Console.WriteLine("Moved To 'TitleScene' Scene.");
        }
        public override void MoveTo() {}
        public override void Initialize() {}
        public override void Close() {}

    }
}